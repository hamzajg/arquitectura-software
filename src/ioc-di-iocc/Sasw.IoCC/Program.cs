﻿using System;
using Microsoft.Extensions.DependencyInjection;

namespace Sasw.IoCC
{
    class Program
    {
        static void Main(string[] args)
        {
            // Service registration
            var serviceProvider =
                new ServiceCollection()
                    .AddSingleton<IPeopleRepository, SqlRepository>()
                    //.AddSingleton<IPeopleRepository, NoSqlRepository>()
                    .AddTransient<ILogger, ConsoleLogger>()
                    .AddTransient<MyProgram>()
                    .BuildServiceProvider();
            
            var myProgram = serviceProvider.GetService<MyProgram>();
            myProgram.DoStuff();
        }
    }

    public class MyProgram
    {
        private readonly IPeopleRepository _peopleRepository;
        private readonly ILogger _logger;

        public MyProgram(
            IPeopleRepository peopleRepository,
            ILogger logger)
        {
            _peopleRepository = peopleRepository;
            _logger = logger;
        }

        public void DoStuff()
        {
            var people = _peopleRepository.GetPeople();
            foreach (var person in people)
            {
                Console.WriteLine($"Person {person.Name}");
            }

            var logger = _logger;
        }
    }
}