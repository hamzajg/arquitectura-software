using System.Collections.Generic;

namespace Sasw.IoCC
{
    public interface IPeopleRepository
    {
        public IEnumerable<Person> GetPeople();
    }
}