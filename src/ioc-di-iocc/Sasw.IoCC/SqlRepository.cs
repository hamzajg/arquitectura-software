using System.Collections.Generic;

namespace Sasw.IoCC
{
    public class SqlRepository
        : IPeopleRepository
    {
        private readonly ILogger _logger;

        public SqlRepository(ILogger logger)
        {
            _logger = logger;
        }
        public IEnumerable<Person> GetPeople()
        {
            var people =
                new List<Person>
                {
                    new Person("Joe"), 
                    new Person("Jane")
                };
            _logger.Log("People retrieved from SQL successfully!");
            return people;
        }
    }
}