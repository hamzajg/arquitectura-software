namespace Sasw.ServiceLocator
{
    public interface ILogger
    {
        void Log(string message);
    }
}