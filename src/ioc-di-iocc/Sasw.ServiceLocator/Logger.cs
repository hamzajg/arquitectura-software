using System;

namespace Sasw.ServiceLocator
{
    public class Logger
        : ILogger
    {
        public void Log(string message)
        {
            Console.WriteLine(message);
        }
    }
}