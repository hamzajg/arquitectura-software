﻿using System;

namespace Sasw.IoC
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("The flow control is a bit inverted!");
            var calculator = MyInstanceFactory<Calculator>.Create();
            var persister = MyInstanceFactory<Persister>.Create();
            calculator.Calculate(123, 456, result => persister.Persist(result));
        }
    }

    public class Calculator
    {
        public void Calculate(int firstValue, int secondValue, Action<int> persister)
        {
            Console.WriteLine("Here it goes some complex calculation that returns some value..");
            var result = firstValue + secondValue;
            persister.Invoke(result);
        }
    }

    public class Persister
    {
        public void Persist(int result)
        {
            Console.WriteLine($"{result} persisted successfully");
        }
    }

    public static class MyInstanceFactory<T> 
        where T : new()
    {
        public static T Create()
        {
            return new T();
        }
    }
}