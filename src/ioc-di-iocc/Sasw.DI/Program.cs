﻿using System;

namespace Sasw.DI
{
    class Program
    {
        static void Main(string[] args)
        {
            // ugly instantiation ..but somebody has to do it!
            ILogger logger = new ConsoleLogger();
            ILogger newLogger = new DatabaseLogger(new DatabaseConnection());
            var myComponent = new MyComponent(logger); //injection of dependency with constructor
            
            myComponent.DoThings();
            myComponent.ChangeLogger(newLogger); //injection of dependency with method
            myComponent.DoThings();
        }
    }

    public class MyComponent
    {
        private ILogger _logger;

        public MyComponent(ILogger logger)
        {
            _logger = logger;
        }

        public void DoThings()
        {
            // do some things and log
            _logger.Log("All done!");
        }

        public void ChangeLogger(ILogger newLogger)
        {
            _logger = newLogger;
        }
    }
}