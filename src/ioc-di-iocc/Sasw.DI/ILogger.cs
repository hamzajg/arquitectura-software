namespace Sasw.DI
{
    public interface ILogger
    {
        void Log(string message);
    }
}