using System;

namespace Sasw.DI
{
    public class DatabaseLogger
        : ILogger
    {
        private readonly DatabaseConnection _databaseConnection;

        public DatabaseLogger(DatabaseConnection databaseConnection)
        {
            _databaseConnection = databaseConnection;
        }
        
        public void Log(string message)
        {
            _databaseConnection.Write(message);
        }
    }

    public class DatabaseConnection
    {
        public void Write(string message)
        {
            // not really implemented, it's just a sample
            Console.WriteLine("All done written on database");
        }
    }
}