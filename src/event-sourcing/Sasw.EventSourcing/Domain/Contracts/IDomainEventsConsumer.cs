using System.Collections;
using System.Collections.Generic;

namespace Sasw.EventSourcing.Domain.Contracts
{
    public interface IDomainEventsConsumer
    {
        void Consume(AggregateRoot aggregateRoot, IEnumerable<IDomainEvent> domainEvents);
    }
}