using System;
using Sasw.EventSourcing.Domain.Contracts;

namespace Sasw.EventSourcing.Domain.ProductAggregate.Events
{
    public class ProductCreated
        : IDomainEvent
    {
        public Guid AggregateId { get; }
        public string Name { get; }

        public ProductCreated(Guid aggregateId, string name)
        {
            AggregateId = aggregateId;
            Name = name;
        }
    }
}