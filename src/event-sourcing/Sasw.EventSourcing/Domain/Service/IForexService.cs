namespace Sasw.EventSourcing.Domain.Service
{
    public interface IForexService
    {
        decimal GetCurrentUsdExchangeRate(string currencyCode);

        void SetEurExchangeRate(decimal exchangeRate);
    }
}