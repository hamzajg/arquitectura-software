using System;
using System.Collections.Generic;
using Sasw.EventSourcing.Domain.Contracts;

namespace Sasw.EventSourcing.Domain
{
    public class AggregateRootFactory<TAggregateRoot>
        where TAggregateRoot : AggregateRoot
    {
        public TAggregateRoot Create(Guid aggregateId, IEnumerable<IDomainEvent> domainEvents)
        {
            var aggregateRoot =
                Activator.CreateInstance(typeof(TAggregateRoot), aggregateId, domainEvents);
            return (TAggregateRoot) aggregateRoot;
        }
    }
}