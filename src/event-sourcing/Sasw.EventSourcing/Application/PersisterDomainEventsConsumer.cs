using System.Collections;
using System.Collections.Generic;
using Sasw.EventSourcing.Application.Contracts;
using Sasw.EventSourcing.Domain;
using Sasw.EventSourcing.Domain.Contracts;

namespace Sasw.EventSourcing.Application
{
    public class PersisterDomainEventsConsumer
        : IDomainEventsConsumer
    {
        private readonly IEventStore _eventStore;

        public PersisterDomainEventsConsumer(IEventStore eventStore)
        {
            _eventStore = eventStore;
        }

        public void Consume(AggregateRoot aggregateRoot, IEnumerable<IDomainEvent> domainEvents)
        {
            _eventStore.PersistEvents(aggregateRoot.Id.ToString(), domainEvents);
        }
    }
}