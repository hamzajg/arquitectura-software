using System;
using Sasw.EventSourcing.Domain;

namespace Sasw.EventSourcing.Application.Contracts
{
    public interface IAggregateRepository<out TAggregateRoot>
        where TAggregateRoot : AggregateRoot
    {
        TAggregateRoot Get(Guid aggregateId);
    }
}