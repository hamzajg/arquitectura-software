using System;
using Sasw.EventSourcing.Application.Commands;
using Sasw.EventSourcing.Application.Contracts;
using Sasw.EventSourcing.Domain.Contracts;
using Sasw.EventSourcing.Domain.ProductAggregate;
using Sasw.EventSourcing.Domain.Service;

namespace Sasw.EventSourcing.Application.CommandHandlers
{
    public class SetPriceHandler
        : ICommandHandler<SetPrice>
    {
        private readonly IAggregateRepository<Product> _productRepository;
        private readonly IDomainEventsConsumer _domainEventsConsumer;
        private readonly IForexService _forexService;

        public SetPriceHandler(
            IAggregateRepository<Product> productRepository,
            IDomainEventsConsumer domainEventsConsumer,
            IForexService forexService)
        {
            _productRepository = productRepository;
            _domainEventsConsumer = domainEventsConsumer;
            _forexService = forexService;
        }
        public Guid Handle(SetPrice setPrice)
        {
            var product = _productRepository.Get(setPrice.AggregateId);
            product.SetPrice("EUR", setPrice.PriceEur, _forexService);
            product.ConsumeDomainEventChanges(_domainEventsConsumer);
            return product.Id;
        }
    }
}