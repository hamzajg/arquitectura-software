using System;

namespace Sasw.ModeloAnemico
{
    public class PersonService
    {
        public Person Drink(Person person, Drink drink)
        {
            if (person.PhysicalState == PhysicalState.Good)
            {
                // no need to drink
                return person;
            }
            
            if (drink == ModeloAnemico.Drink.Whiskey)
            {
                if (person.Age < 18)
                {
                    throw new Exception("Underage can't drink whiskey");
                }
                
                person.PhysicalState = PhysicalState.AlcoholDrunk;
                return person;
            }
            
            if (drink == ModeloAnemico.Drink.Water && person.Age < 1)
            {
                throw new Exception("Babies can't drink water, just milk");
            }
            
            person.PhysicalState = PhysicalState.Good;
            return person;
        }
    }
}