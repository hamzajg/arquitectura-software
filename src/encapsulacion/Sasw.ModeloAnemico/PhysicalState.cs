namespace Sasw.ModeloAnemico
{
    public enum PhysicalState
    {
        Good,
        Thirsty,
        AlcoholDrunk
    }
}