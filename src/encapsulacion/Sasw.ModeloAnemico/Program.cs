﻿using System;

namespace Sasw.ModeloAnemico
{
    class Program
    {
        static void Main(string[] args)
        {
            var adult = 
                new Person
                {
                    Name = "Joe", 
                    Age = 36, 
                    PhysicalState = PhysicalState.Thirsty
                };
            
            var personService = new PersonService();
            personService.Drink(adult, Drink.Whiskey);
            Console.WriteLine($"{adult.Name} is now {adult.PhysicalState}");
            
            // DANGEROUS THINGS!
            for (var i = 0; i <= 10; i++)
            {
                adult.PhysicalState = PhysicalState.Thirsty;
                personService.Drink(adult, Drink.Whiskey);
                Console.WriteLine($"{adult.Name} is even more {adult.PhysicalState}");
            }
            
            adult.Name = null;
            adult.Age = -1;

            var baby =
                new Person
                {
                    Age = 0,
                    PhysicalState = PhysicalState.Thirsty
                };
            baby.PhysicalState = PhysicalState.AlcoholDrunk;
        }
    }
}