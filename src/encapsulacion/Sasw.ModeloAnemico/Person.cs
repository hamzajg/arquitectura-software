namespace Sasw.ModeloAnemico
{
    public class Person
    {
        public string Name { get; set; }
        public int Age { get; set; }
        public PhysicalState PhysicalState { get; set; }
    }
}