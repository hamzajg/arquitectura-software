using System;

namespace Sasw.PatronEstrategia
{
    public class Puncher
        : IFightingBehavior
    {
        public void Fight()
        {
            Console.WriteLine("Toma puñetazo!");
        }
    }
}